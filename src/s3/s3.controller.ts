import {
  Body,
  Controller,
  HttpStatus,
  Post,
  UploadedFile,
  UseInterceptors,
  UsePipes,
} from '@nestjs/common';
import { S3Service } from './s3.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiConsumes } from '@nestjs/swagger';
import { UploadFileDto } from './dto/upload-file.dto';
import { SharpPipe } from './pipe/sharp.pipe';
import { CustomFileInterceptor } from './file.interceptor';

@Controller()
export class S3Controller {
  constructor(private readonly s3Service: S3Service) {}

  @Post('upload-file')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(CustomFileInterceptor)
  @UseInterceptors(FileInterceptor('file'))
  @UsePipes(new SharpPipe())
  async uploadFile(
    @Body() body: UploadFileDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    try {
      const res = await this.s3Service.uploadFile(body);
      return {
        statusCode: HttpStatus.OK,
        entity: res,
      };
    } catch (err) {
      return {
        statusCode: err.code,
        errorMessage: err.message,
      };
    }
  }
}
