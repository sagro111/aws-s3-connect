import { DiskStorageOptions } from 'multer';
import { v4 as uuid } from 'uuid';

export const editFileName: DiskStorageOptions['filename'] = (
  req,
  file,
  callback,
) => {
  const name = file.originalname.split('.')[0];
  const fileExtName = '.' + file.originalname.split('.')[1];
  const randomName = uuid();
  callback(null, `${name}-${randomName}${fileExtName}`);
};
