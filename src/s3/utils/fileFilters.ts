import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { HttpException, HttpStatus } from '@nestjs/common';
import { extname } from 'path';

export const fileFilter: MulterOptions['fileFilter'] = (
  req,
  file,
  callback,
) => {
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif|svg|mp3)$/)) {
    return callback(
      new HttpException(
        `Unsupported file type ${extname(file.originalname)}`,
        HttpStatus.BAD_REQUEST,
      ),
      false,
    );
  }
  callback(null, true);
};
