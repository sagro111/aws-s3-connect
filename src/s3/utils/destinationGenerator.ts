import { DiskStorageOptions } from 'multer';
import { mkdirSync } from 'fs';

export const destinationGenerator: DiskStorageOptions['destination'] = (
  req,
  file,
  cb,
) => {
  const defaultPath = `./uploads/`;
  const path =
    defaultPath + [req.body.prefix, req.body.id].filter((i) => i).join('/');
  mkdirSync(path, { recursive: true });
  cb(null, path);
};
