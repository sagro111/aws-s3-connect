import {Test, TestingModule} from "@nestjs/testing";
import {S3Service} from "./s3.service";
import {
  createAwsServiceMock,
  createAwsServicePromisableSpy,
  getAwsServiceMock
} from "nest-aws-sdk/dist/testing/aws-service-mocks";
import {S3} from "aws-sdk";

describe('S3ManagerService', () => {
  describe('listBucketContents()', () => {
    it('should call the list method and return the Content keys', async () => {
      const module: TestingModule = await Test.createTestingModule({
        providers: [
          S3Service,
          createAwsServiceMock(S3, {
            useValue: {
              listObjectsV2: () => null,
            }
          }),
        ],
      }).compile();

      const service = module.get(S3Service);

      const listSpy = createAwsServicePromisableSpy(
          getAwsServiceMock(module, S3),
          'listObjectsV2',
          'resolve',
          {
            Contents: [ { Key: 'myKey' } ],
          },
      );

      const result = await service.listBucketContents('myBucket');

      expect(result.length).toBe(1);
      expect(result[0]).toBe('myKey');
      expect(listSpy).toHaveBeenCalledTimes(1);
      expect(listSpy).toHaveBeenCalledWith({ Bucket: 'myBucket' });
    });
  })
});
