import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class UploadFileDto {
  @ApiProperty({
    required: true,
    description: `
            Prefix for filepath: user-avatars | words | russian-content
        `,
  })
  @IsNotEmpty()
  @IsString()
  prefix: 'user-avatars' | 'words' | 'russian-content';

  @ApiProperty({
    required: true,
    description: ` 
            Entities ID, if upload avatar this will be userId
        `,
  })
  @IsString()
  id: string;

  @ApiProperty({
    required: true,
    type: 'string',
    format: 'binary',
  })
  @IsNotEmpty()
  @IsOptional({ each: true, message: 'File is corrupated' })
  @Type(() => Buffer)
  file: {
    /** Field name specified in the form */
    fieldname: string;
    /** Name of the file on the user's computer */
    originalname: string;
    /** Encoding type of the file */
    encoding: string;
    /** Mime type of the file */
    mimetype: string;
    /** Size of the file in bytes */
    size: number;
    /** The folder to which the file has been saved (DiskStorage) */
    destination: string;
    /** The name of the file within the destination (DiskStorage) */
    filename: string;
    /** Location of the uploaded file (DiskStorage) */
    path: string;
    /** A Buffer of the entire file (MemoryStorage) */
    buffer: Buffer;
  };
}
