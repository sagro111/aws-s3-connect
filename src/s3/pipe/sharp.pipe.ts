import {
  ArgumentMetadata,
  BadRequestException,
  Inject,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import * as path from 'path';
import * as sharp from 'sharp';
import * as fs from 'fs';
import { mkdir, mkdirSync } from 'fs';

type Thumb = [width: number, height: number];
const THUMBS_SIZE: Array<Thumb> = [
  [120, 120],
  [700, 700],
  [1200, 1200],
];

const generateFileName = (
  prefix: string,
  originalName: string,
  thumb?: Thumb,
) => {
  const thumbName = thumb !== undefined ? `-bfthumb-${thumb.join('x')}` : '';
  return `${prefix}/${Date.now()}-${originalName}${thumbName}.webp`;
};

@Injectable()
export class SharpPipe implements PipeTransform<any, Promise<void>> {
  async transform(file, { type }: ArgumentMetadata): Promise<void> {
    if (type === 'custom') {
      const defaultPath = `./uploads/${file.prefix}/${file.id}/`;

      mkdirSync(defaultPath, { recursive: true });

      const originalName = path.parse(file.originalname).name;
      const originalFileName = generateFileName(defaultPath, originalName);

      await Promise.all([
        ...THUMBS_SIZE.map((thumb: Thumb) => {
          const filename = generateFileName('', originalName, thumb);
          sharp(file.buffer)
            .resize(thumb[0], thumb[1], { fit: 'cover' })
            .webp({ effort: 2 })
            .toFile(path.join(defaultPath, filename));
        }),
        sharp(file.buffer).webp({ effort: 2 }).toFile(originalFileName),
      ]);
    }
    return file;
  }
}
