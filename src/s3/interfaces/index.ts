import { PutObjectRequest } from 'aws-sdk/clients/s3';

export interface UploadS3Props {
  files: string[];
  path: string;
  params?: PutObjectRequest;
}

export interface UploadS3Response {
  url: string;
}
