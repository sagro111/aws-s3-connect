import { Module } from '@nestjs/common';
import { S3Service } from './s3.service';
import { S3Controller } from './s3.controller';
import { AwsSdkModule } from 'nest-aws-sdk';
import { S3 } from 'aws-sdk';
import { MulterModule } from '@nestjs/platform-express';
import { memoryStorage } from 'multer';

@Module({
  imports: [
    AwsSdkModule.forFeatures([S3]),
    MulterModule.register({
      storage: memoryStorage(),
    }),
  ],
  providers: [S3Service],
  controllers: [S3Controller],
})
export class S3Module {}
