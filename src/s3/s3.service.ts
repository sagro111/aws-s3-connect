import { Injectable, Logger } from '@nestjs/common';
import { InjectAwsService } from 'nest-aws-sdk';
import { S3 } from 'aws-sdk';
import { ConfigService } from '@nestjs/config';
import * as fs from 'fs';
import { UploadS3Props, UploadS3Response } from './interfaces';
import { UploadFileDto } from './dto/upload-file.dto';

@Injectable()
export class S3Service {
  private readonly bucket: string;

  constructor(
    @InjectAwsService(S3) private readonly s3: S3,
    configService: ConfigService,
  ) {
    this.bucket = configService.get<string>('S3Bucket');
  }

  private async uploadS3({
    files,
    path,
    params,
  }: UploadS3Props): Promise<UploadS3Response> {
    try {
      const response = await Promise.all(
        files.map(async (fileName) => {
          const file = fs.readFileSync(`./${path}/${fileName}`);

          const { Location } = await this.s3
            .upload({
              Bucket: this.bucket,
              Key: `${path}/${fileName}`,
              ACL: 'public-read',
              Body: file,
              ...params,
            })
            .promise();
          return Location;
        }),
      );

      const mainUr = response.find((url) => !url.includes('bfthumb'));

      return {
        url: mainUr,
      };
    } catch (err) {
      Logger.error(err);
      throw Error(err.message);
    } finally {
      fs.rmSync(path, { recursive: true });
    }
  }

  async uploadFile({ prefix, id }: UploadFileDto) {
    const path = `uploads/${prefix}/${id}`;
    const files = fs.readdirSync(`./${path}`);

    return this.uploadS3({ files, path });
  }
}
