import { Module } from '@nestjs/common';
import { AwsSdkModule } from 'nest-aws-sdk';
import { Credentials } from 'aws-sdk';
import { S3Module } from './s3/s3.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [],
    }),
    S3Module,
    AwsSdkModule.forRootAsync({
      defaultServiceOptions: {
        useFactory: () => {
          return {
            region: 'us-west-1',
            credentials: new Credentials({
              accessKeyId: process.env.accessKeyId,
              secretAccessKey: process.env.secretAccessKey,
            }),
          };
        },
      },
    }),
  ],
})
export class AppModule {}
