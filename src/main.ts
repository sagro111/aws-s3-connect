import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";
import {Logger, ValidationPipe} from "@nestjs/common";

const checkEnv = () => process.env.secretAccessKey === undefined ||
    process.env.S3Bucket === undefined ||
    process.env.accessKeyId === undefined

async function bootstrap() {
    if (checkEnv()) {
        Logger.error('You doest not set env variables')
        return
    }

    const app = await NestFactory.create(AppModule);
    app.setGlobalPrefix('api');
    app.useGlobalPipes(new ValidationPipe());

    const config = new DocumentBuilder()
        .setTitle('Filestorage AWS S3')
        .setDescription('The filestorage API description')
        .setVersion('1.0')
        .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api/doc', app, document);

    await app.listen(3000);
}

bootstrap();


